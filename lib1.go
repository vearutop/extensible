package extensible // import "gitlab.com/vearutop/extensible"
import "gitlab.com/vearutop/extensible/internal"

type iSomething interface {
	// DoWhatever does whatever it does
	DoWhatever(self iSomething) (error, iSomething)
	// GetParam gets param
	GetParam() string
}

// New creates something
func New(param string) iSomething { return &myPrivate{Param: param} }

type myPrivate struct {
	Param string `json:"param" db:"param"`
}

// DoWhatever does whatever it does
func (m myPrivate) DoWhatever(a iSomething) (error, iSomething) {
	return internal.Helper{"EN"}.GetError("ouch"), New(a.GetParam())
}

// GetParam gets param
func (m myPrivate) GetParam() string { return m.Param }
