package internal

import "errors"

type Helper struct {
	Lang string
}

func (h Helper) GetError(text string) error {
	return errors.New(h.Lang + text)
}
